package AssignmentOne;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class Lib 
{
	

	private  JavascriptExecutor js;
	 String pageLoadStatus = null;
	
	
	/**
	 * Method accepts browser name and return driver instance of that browser driver
	 * @param browser
	 * @return
	 */
	public WebDriver getWebDriverInstance(String browser) 
	{
		WebDriver driver = null;
		try {

		if (browser.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\resources\\chromedriver.exe");
			driver = new ChromeDriver(); 
			driver.manage().window().maximize();
			//driver.get(URL); // Launch the URL
			
		} 
		else if(browser.equalsIgnoreCase("FF") || browser.equalsIgnoreCase("firefox"))
		{
			 System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\resources\\geckodriver.exe");
			 driver = new FirefoxDriver();
			 //driver.manage().window().maximize();
			 //driver.get(URL); // Launch the URL 
		}
		
		
		}catch (Exception e){
			System.out.println("There is some problme in getting the driver instance, please see exception for details");
			e.printStackTrace();
			
		}
		return driver;
		
    }
	/**
	 * Navigate to book flights page
	 * @param driver
	 */
	public  void NavigateToBookFlight(WebDriver driver)
	{
		try{
			driver.findElement(By.xpath(Locators.xpath_flightLinkOnHomePage)).click();
		}catch (Exception e){
			System.out.println("There is some problme in navigation on home page to flight menu, please see exception for details");
			e.printStackTrace();
			
		}
		
	}
	
	/**
	 * Set Source Place
	 * @param driver
	 * @param source
	 * @throws AWTException 
	 */
	
	public  void SetSource(WebDriver driver, String source) throws AWTException
	{
		try {
		driver.findElement(By.xpath(Locators.xpath_Source)).sendKeys(source);
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);
		}catch (Exception e){
			System.out.println("There is some problme in selecting source of your journey, please see exception for details");
			e.printStackTrace();
			
		}
	}
	
	/**
	 * Set Destination Place Name
	 * @param driver
	 * @param destination
	 * @throws AWTException 
	 */
	public  void SetDestination(WebDriver driver, String destination) throws AWTException
	{
		try{
		driver.findElement(By.xpath(Locators.xpath_Destination)).sendKeys(destination);
		Robot rb=new Robot();
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		}catch (Exception e){
			System.out.println("There is some problme in selecting destination of your journey, please see exception for details");
			e.printStackTrace();
			
		}
	}
	
	/**
	 * Set Traveling date from
	 * @param driver
	 * @param DateFrom
	 */
	public void SetDateFrom(WebDriver driver,String DateFrom )
	{
		try {
		//driver.findElement(By.xpath(Locators.xpath_SetFromDate)).sendKeys(DateFrom);
		TypeInField(driver, Locators.xpath_SetFromDate, DateFrom);
		Robot rb=new Robot();
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		}catch (Exception e){
			System.out.println("There is some problme in selecting starting date of your journey, please see exception for details");
			e.printStackTrace();
			
		}

	}
	
	/**
	 * Set Traveling date to
	 * @param driver
	 * @param DateTo
	 */
	public  void SetDateTo(WebDriver driver,String DateTo )
	{
		
		try{
			// driver.findElement(By.xpath(Locators.xpath_SetToDate)).sendKeys(DateTo);
			TypeInField(driver, Locators.xpath_SetToDate, DateTo);
			
		}catch (Exception e){
				e.printStackTrace();
				System.out.println("There is some problme in selecting end date of your journey, please see exception for details");
			
		}
	
	}
	/**
	 * This method will find the flight by pressing 'find flight' button
	 */
	public  void SearchFlightsAndWaitForResults(WebDriver driver) 
	{
		try {
		driver.findElement(By.xpath(Locators.xpath_BtnSearchFlights)).click();
		// wait for search results to load
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(Locators.classNm_waitingforthisEle)));
		}catch (Exception e){
			if (driver.findElement(By.xpath(Locators.xpath_SearchError)) != null) {
				System.out.println("We couldn't find any flights that match your requirements, please try to refine your search");
				Assert.assertTrue(false);
			}
			System.out.println("There is some problem in searching the results, please see exception for details");
			e.printStackTrace();
			Assert.assertTrue(false, "There is some problem in searching the results, please see exception for details");
		}

	}
	
	/**
	 * This method will return number of flights found for given search
	 */
	public  int getNumberOfFlights(WebDriver driver) 
	{
		
		int temp=0;
		WebElement FlightList=driver.findElement(By.xpath(Locators.xpath_SearchResModuleList));
		List <WebElement> TotalRows=FlightList.findElements(By.tagName(Locators.tagName_SearchResNumberOfModule));
		
		try {
		if(driver.findElements(By.xpath(Locators.xpath_SearchAdvertise1)).size()!=0) {
			temp++;
		} else if (driver.findElements(By.xpath(Locators.xpath_SearchAdvertise2)).size()!=0) {
			temp++;
		}
		}catch (Exception e){
			System.out.println("There is some problme in getting number of flights found in search results, please see exception for details");
			e.printStackTrace();
			
		}
		return TotalRows.size()-temp;
	}
	
	/**
	 * Validate flight details against source and destination appeared in the results
	 */
	public  void validateFlightsDetails(String ActualDetail, WebDriver driver)
	{
		System.out.println("Validation of Search Results has been started");
		
		try {
		for (int i=1; i<=3; i++) {
			String finalPath=Locators.xpath_SearchResFirstHalf+i+Locators.xpath_SearchResSecondHalf;
			String flightDetail=driver.findElement(By.xpath(finalPath)).getText();
			if(flightDetail.equalsIgnoreCase(ActualDetail)) {
				System.out.println("Result of Row "+i+" is Matched");
			}
		}
		}catch (Exception e) {
			System.out.println("There is some problem in validating the results");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * This method will allow entering character one by one in a text field instead of sendkeys
	 * @param xpath
	 * @param value
	 */
	public  void TypeInField(WebDriver driver, String xpath, String value){
	    String val = value; 
	    WebElement element = driver.findElement(By.xpath(xpath));
	    element.clear();

	    for (int i = 0; i < val.length(); i++){
	        char c = val.charAt(i);
	        String s = new StringBuilder().append(c).toString();
	        element.sendKeys(s);
	    }    
	 }
	
	/**
	 * This method will wait for page to load using javascript
	 * This will ensure all the elements in the DOM are completely loaded and DOM is ready
	 */
	 public  void waitForPageToLoad(WebDriver driver) 
	 {
		    do {
		    js = (JavascriptExecutor) driver;
		    pageLoadStatus = (String)js.executeScript("return document.readyState");
		      System.out.print(".");
		    } while ( !pageLoadStatus.equals("complete") );
		    System.out.println();
		    System.out.println("Page Loaded.");
	 }
	 
	/**
	 * This method will take the screenshot and store the screenshot with unique name into project
	 * @param driver
	 */
	 public void takeScreenPrint(WebDriver driver)
	 {
		 //Create a date format for storing screenshot by unique name
		 DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy h-m-s");
	     Date date = new Date();
		 // Take screenshot and store as a file format
		 File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		 try {
		 // now copy the  screenshot to desired location using copyFile //method
		 FileUtils.copyFile(src, new File(System.getProperty("user.dir")+"\\Screenshots\\"+"ScreenSt"+"-"+dateFormat.format(date)+".png"));
		 } catch (IOException e){
			 System.out.println(e.getMessage());
			 }
	 }


}
