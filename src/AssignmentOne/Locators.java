package AssignmentOne;

/**
 * This class stores the locators for required webelement
 * @author Yogesh.Desai
 *
 */
public class Locators 
{
	// Locator for link to flight home page 
	public static String xpath_flightLinkOnHomePage="//*[@id='tab-flight-tab']/span[3]";
	// Locator for Selecting Starting point of your journey
	public static String xpath_Source="//*[@id='flight-origin']";
	// Locator for Selecting end point of your journey
	public static String xpath_Destination="//*[@id='flight-destination']";
	// Locator for selecting the start date of your journey
	public static String xpath_SetFromDate="//*[@id='flight-departing']";
	// Locator for selecting end date if your journey
	public static String xpath_SetToDate="//*[@id='flight-returning']";
	// Locator for 'Search' button
	public static String xpath_BtnSearchFlights="//*[@id='search-button']";
	// Locator for getting the list of searched flights
	public static String xpath_SearchResModuleList="//*[@id='flightModuleList']";
	// Locator for element for which we'll wait to appear in search results
	public static String classNm_waitingforthisEle="loyalty-text";
	//Locator for identifying the row as flight details by tag name li
	public static String tagName_SearchResNumberOfModule="li";
	// Locator for search error message
	public static String xpath_SearchError="//*[@id='ajax-error']/div/div[1]";
	// Locator for Advertisement type1
	public static String xpath_SearchAdvertise1="//*[contains(@class, 'inline-banner')]";
	// Locator for advertisement type2
	public static String xpath_SearchAdvertise2="//*[contains(@class, 'intent-media-ad')]";
	// first half of the locator for locating source-destination in results
	public static String xpath_SearchResFirstHalf="//div[@id='flight-listing-container']/ul/li[";
	// Second half of the locator for locating source-destination in results
	public static String xpath_SearchResSecondHalf="]/div[2]/div/div[1]/div/div[2]/div/div/div[2]/div[2]";

}
