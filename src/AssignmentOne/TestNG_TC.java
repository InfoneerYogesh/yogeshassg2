package AssignmentOne;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;


@Listeners(Listners.class)

/**
 * This class contains test case for flight booking using TestNG parallel execution
 * @author Yogesh.Desai
 * @version 0.1
 * @Date - 18/01/2018
 */

public class TestNG_TC{
	
	public WebDriver driver;
	//public  String URL="https://www.orbitz.com"; // Application URL
	Lib lib=new Lib();
	
	
@Test (description="Flight Booking", dataProvider="FlightDetailsProvider")
public void FlightBooking(String sPlace, String sDate, String dPlace, String dDate) {
	  
	 try {
	 // This will take user to book flight page
	 lib.NavigateToBookFlight(driver); 
	 lib.waitForPageToLoad(driver);
	 // This is Test NG reporter logging. Boolean 'true' will print logs in console as well as in report
	 Reporter.log("Setting Source and Destinations ....", true);
	// Select the Place from where we want to board the flight
	 lib.SetSource(driver, sPlace);
	 // Select the destination where we want to go
	 lib.SetDestination(driver, dPlace);
	 Reporter.log("Setting Start and End Dates ....", true);
	 // Select start date of your journey
	 lib.SetDateFrom(driver, sDate); 
	 // Select end date of your journey
	 lib.SetDateTo(driver, dDate);
	 // Take screenshot
	 lib.takeScreenPrint(driver);
	 //Search the flights and wait for the results to load completely
	 lib.SearchFlightsAndWaitForResults(driver);
	 lib.waitForPageToLoad(driver);
	 //Get the number of rows of the results which will define number of flights for above search
	 int numberOfResults=lib.getNumberOfFlights(driver);
	 System.out.println("Number of flights for above search are :  " +numberOfResults);
	 Reporter.log("Validating Results ....", true);
	 // Validate the flight search results (first 3 rows only) against the source and destination selected
	 lib.validateFlightsDetails("PNQ - BKK", driver);
	 // Take screenshot
	 lib.takeScreenPrint(driver);
	 
    } catch (Exception e){
    	e.printStackTrace();
    	Assert.assertTrue(false, "There is some problem in validating flight booking scenario, Plese check exception for detais");
    }
	
  }
  
  /**
   * This annotation will get the parameter from the test NG suit.
   * In below case we need to pass 'browser' name as a parameter in testNg suit
   */
  @Parameters({ "browser", "URL" })
  
  @BeforeMethod  
  /**
   * This method will get the driver instance as per the 'browser' name 
   * passed as parameter e.g FF - for firefox & chrome - for Chrome 
   * and launch the application URL
   * 
   * @param browser
   */
  public void beforeMethod(String browser, String URL) 
  {
	  System.out.println("Browser to be used is :"+browser);
	  driver=lib.getWebDriverInstance(browser);
	  driver.get(URL);
	  Reporter.log("Application '"+URL+"' Started ....", true);
	  
	  	 
  }

  @AfterMethod
  /**
   * This method will close the current browser instance
   */
  public void TearDown() {
	  	driver.close();
	  	Reporter.log("Application Closed ....", true);
	}
  
  
  /**
   * @return Object[][] 
   */

  @DataProvider(name="FlightDetailsProvider")
  public Object[][] getDataFromDataprovider(){
  return new Object[][] 
  	{
          { "Pune, India (PNQ)", "02/22/2018", "Bangkok, Thailand (BKK-Suvarnabhumi Intl.)", "02/25/2018" },
         
    };
  }

}
